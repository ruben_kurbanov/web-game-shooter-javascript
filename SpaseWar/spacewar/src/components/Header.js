import React from 'react'
import { Link } from 'react-router-dom';

import Login from "./account/Login";
import Registration from "./account/Registration";

class Header extends React.Component {
    render() {
        return (
            <header>
                <Link to="/">
                    <div className="header-link_home">
                        <h1>Domácí knihovna</h1>
                    </div>
                </Link>
                <p className='header-login_registration'>
                    <Link className="header-link_login" to="/login">Přihlásit se/</Link>
                    <Link className="header-link_register" to="/registration">Registrovat</Link>
                </p>
            </header>
        )

    }
}

export default Header