import React from 'react'
import { Link } from 'react-router-dom'

import '../cssComponent/Footer.css';

class Footer extends React.Component {
    render() {
        return (
            <footer className = "footer">
                <p>
                <Link to="/">Home</Link>
                |
                <Link to="/settings">Settings</Link>
                |
                <Link to="/developed">Developed By</Link>
                </p>
            </footer>

        )
    }
}

export default Footer