import React from 'react';
import '../../css/account/Setting.css';

export default class Setting extends React.Component {
    differentPasswords = false
    
    constructor(props) {
        super(props);
        this.state = {
            loaded: false,
            forename: "",
            surname: "",
            email: "",
            passActual: "",
            pass1: "",
            pass2: "",

        }

        this.forenameActual =  "";
        this.surnameActual =  "";
        this.emailActual =  "";
        this.passActualActual = "";

        this.loadData();
        this.onClick = this.onClick.bind(this);

    }

    componentDidMount() {
        this.setState({
            loaded: true

        })

    }

    setForename(text) {
        if(text.length !== 0){
            this.setState({
                forename: text
            })
        }
    }

    setSurname(text) {
        if(text.length !== 0){
            this.setState({
               surname: text
            })
        }

    }

    setEmail(text) {
        if(text.length !== 0){
            this.setState({
               email: text
            })
        }

    }

    setPassActual(text) {
        if(text.length !== 0){
            this.setState({
                passActual: text
            })
        }

    }

    loadData(){

        //from db
        this.forenameActual = "name";
        this.surnameActual = "sname";
        this.emailActual = "mailmail";
        this.passActualActual = "ddddddddd";


        this.l();
    }

    l(){
        this.setState ({
            forename:this.forenameActual,
            surname:this.surnameActual,
            email:this.emailActual,
            passActual:this.passActualActual

        })
    }

    setPass1(text) {
        if(text.length !== 0){
            this.setState({
                pass1: text
            })
        }


    }

    setPass2(text) {
        if(text.length !== 0){
            this.setState({
                pass2: text
            })
        }

    }

    onClick(e) {

       this.checkPassEquility();
       this.checkActualPassEquility();

       alert(this.state.forename + " " + this.state.surname + " " + this.state.email);
       e.preventDefault();
    }

    checkPassEquility() {
        if (this.state.pass1 !== this.state.pass2) {
            alert("Hesla se neshodují!");
        }
    }

    checkActualPassEquility(){
        if(this.passActualActual !== this.state.passActual && this.state.passActual.length !== 0 ){
            alert("Aktuální heslo není správné!");
        }
    }

    render() {
        return (
            <main>
                <div className="main-div">
                    <h1 className="head-login_settings">
                        <a className="link_setings"></a>
                    </h1>

                    <form>
                        <fieldset className = "field">
                            <legend>Nastavení</legend>
                            <h1>Osobní</h1>

                            <p>Jméno:
                                <a class = "name">{this.forenameActual}</a>
                            </p>

                            <p>Zadejte svoje nové jméno
                                <input onChange={e => this.setForename(e.target.value)}
                                className="name_change" type="text"></input>
                            </p>

                            <hr></hr>
                            <p>Příjmení: <a class = "surname">{this.surnameActual}</a></p>
                                <p>Zadejte svoje nové příjmění<input className="surname_change"
                                 onChange={e => this.setSurname(e.target.value)} type="text"></input></p>

                            <hr></hr>
                            <p>Email:
                                <a class = "email">{this.emailActual}</a>
                            </p>
                            <p>Zadejte svůj nový email
                                <input className="email_change"
                                 onChange={e => this.setEmail(e.target.value)} type="text"></input>
                            </p>

                            <hr></hr>
                            <p>Heslo:

                            </p>
                            <p>Zadejte svoje aktuální heslo
                                <input className="actual_p"
                                onChange={e => this.setPassActual(e.target.value)} type="password"></input>
                            </p>
                            <p>Zadejte svoje nové heslo
                                <input className="p1_change"
                                 onChange={e => this.setPass1(e.target.value)} type="password"></input>
                            </p>

                            <p>Opětovné heslo
                                <input className="p1_change"
                                 onChange={e => this.setPass2(e.target.value)} type="password"></input>
                            </p>

                            <h1>Design</h1>
                            <input type="checkbox" name="design" value="night" ></input>Noční režim

                            <button className="submit" onClick = {this.onClick} type="submit">Změnit</button>
                        </fieldset>

                    </form>

                </div>
            </main>
        )
    }
}