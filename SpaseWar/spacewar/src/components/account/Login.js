import React from 'react';
import { Link } from 'react-router-dom';

import '../../css/account/LoginRegistration.css';
import App from '../../App.js'

export default class Login extends React.Component {
    wrongEmail = false
    wrongPassword = false

    constructor(props) {
        super(props);
        this.state = {
            loaded: false,
            email: "",
            password: "",
        }
    }

    componentDidMount() {
        this.setState({
            loaded: true
        })
    }

    setEmail(text) {
        this.setState({
            email: text
        })
    }

    setPassword(text) {
        this.setState({
            password: text
        })
    }
//
//    hashPassword() {
//        return sha256(this.state.password.toString())
//    }

    async loginUser() {
        return fetch('http://localhost:8080/api/auth', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: {
                "email": this.state.email,
                "password": this.hashPassword()
            }
        })
        .then(data => data.json())
    }

    handleSubmit = async e => {
        e.preventDefault();
        const token = await this.loginUser();
        App.setToken(token);
        this.props.history.push("/")
    }

    render() {
        return (
            <main>
                <div className="main-div">
                    <h1 className="head-login-registration">
                        <Link className="link-login" to="/login" state="selected">
                            Přihlášení
                        </Link>
                        |
                        <Link className="link-registration" to="/registration" state="unselected">
                            Registrace
                        </Link>
                    </h1>
                    <form onSubmit={this.handleSubmit}>
                        <label>
                            <p>E-mail</p>
                            <p className="error-message"  hidden={!this.wrongEmail}>
                                ! Uživatel s touto e-mailovou adresou není v databázi !
                            </p>
                            <input className="email" type="text" placeholder="priklad@mail.cz"
                                onChange={e => this.setEmail(e.target.value)}/>
                        </label>
                        <label>
                            <p>Heslo</p>
                            <p className="error-message"  hidden={!this.wrongPassword}>
                                ! Nesprávné heslo !
                            </p>
                            <input className="password" type="password" placeholder="******"
                                onChange={e => this.setPassword(e.target.value)}/>
                        </label>
                        <div className="div-button">
                            <button className="login-button" type="submit">Přihlásit se</button>
                        </div>
                    </form>
                </div>
            </main>
        )
    }
}