import React from 'react';
import { Link } from 'react-router-dom';

import '../../css/account/LoginRegistration.css';
import App from '../../App.js'

export default class Registration extends React.Component {
    tooShortPassword = false
    differentPasswords = false
    wrongEmailFormat = false

    constructor(props) {
        super(props);
        this.state = {
            loaded: false,
            forename: "",
            surname: "",
            email: "",
            pass1: "",
            pass2: "",
        }
    }

    componentDidMount() {
        this.setState({
            loaded: true
        })
    }

    setForename(text) {
        this.setState({
            forename: text
        })
    }

    setSurname(text) {
        this.setState({
            surname: text
        })
    }

    setEmail(text) {
        //TODO check for '@' and at least 1 '.' and possibly show error message
        this.setState({
            email: text
        })
        this.wrongEmailFormat = true
    }

    setPass1(text) {
        this.setState({
            pass1: text
        })
        if (text.length > 5) {
            this.tooShortPassword = false
        } else {
            this.tooShortPassword = true
        }
        this.checkPassEquility()
    }

    setPass2(text) {
        this.setState({
            pass2: text
        })
        this.checkPassEquility()
    }

    checkPassEquility() {
        if (this.state.pass1 === this.state.pass2) {
            this.differentPasswords = false
        } else {
            this.differentPasswords = true
        }
    }

//    hashPassword() {
//        return sha256(this.state.pass1.toString())
//    }

    async registerUser() {
        return fetch('http://localhost:8080/api/user/register', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                "firstname": this.state.firstname,
                "surname": this.state.surname,
                "email": this.state.email,
                "password": this.hashPassword()
            })
        })
        .then(response => response.json())
        .then(data => data.json())
    }

    handleSubmit = async e => {
        e.preventDefault();
        const token = await this.registerUser();
        App.setToken(token);
        this.props.history.push("/")
    }

    render() {
        return (
            <main>
                <div className="main-div">
                    <h1 className="head-login-registration">
                        <Link className="link-login" to="/login" state="unselected">Přihlášení</Link>
                        |
                        <Link className="link-registration" to="/registration" state="selected">Registrace</Link>
                    </h1>
                    <form className="registration-form" onSubmit={this.handleSubmit}>
                        <label className="label-forename">
                            <p>Křestní jméno</p>
                            <input className="forename" type="text" placeholder="Jméno"
                                defaultValue={this.state.forename} onChange={e => this.setForename(e.target.value)}/>
                        </label>
                        <label className="label-surname">
                            <p>Příjmení</p>
                            <input className="surname" type="text" placeholder="Příjmení"
                                defaultValue={this.state.surname} onChange={e => this.setSurname(e.target.value)}/>
                        </label>
                        <label className="label-email">
                            <p>E-mail</p>
                            <p className="error-message"  hidden={!this.wrongEmailFormat}>! Toto není validní formát e-mailové adresy !</p>
                            <input className="email" type="text" placeholder="priklad@mail.cz"
                                defaultValue={this.state.email} onChange={e => this.setEmail(e.target.value)}/>
                        </label>
                        <label className="label-password1">
                            <p>Heslo</p>
                            <p className="error-message"  hidden={!this.tooShortPassword}>! Heslo je příliš krátké (minimum je 6 znaků) !</p>
                            <input className="password1" type="password" placeholder="******"
                                onChange={e => this.setPass1(e.target.value)}/>
                        </label>
                        <label className="label-password2">
                            <p>Opětovné heslo</p>
                            <p className="error-message" hidden={!this.differentPasswords}>! Hesla se neshodují !</p>
                            <input className="password2" type="password" placeholder="******"
                                onChange={e => this.setPass2(e.target.value)}/>
                        </label>
                        <div className="div-button">
                            <button className="register-button" type="submit">Zaregistrovat</button>
                        </div>
                    </form>
                </div>
            </main>
        )
    }
}