import React from 'react'
import './../../css/search/search.css'
import SearchResult from "./SearchResult";
import booksData from "./BooksData";

class Search extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoaded: false,
            data: booksData
        }
        this.handleInputChange.bind(this)
        this.handleSubmit.bind(this)
    }

    handleInputChange(event) {
        const value = event.target.type === 'checkbox' ? event.target.checked : event.target.value;
        const name = event.target.name;
        this.setState({
            [name]: value
        });
        event.preventDefault();
    }

    handleSubmit(event) {
        console.log();
        event.preventDefault();
        /*
        fetch("localhost:8080/api/books/all")
            .then(result => result.json())
            .then(result => {
                    this.setState({
                        isLoaded: true,
                        data: result.books
                    })
                },
                error => {
                    this.setState({
                        isLoaded: true,
                        error
                    })
                });

         */
    }

    render() {
        const searchResults = this.state.data.map(result =>
            <SearchResult
                key={result.id}
                resultType={"book-result"}
                imageSource={result.imageSource}
                title={result.title}
                author={result.author}
                year={result.year}
                owner={result.owner}
            />
        );
        return (
            <main>
                <div className={"search-bar"}>
                    <p>Hledám:</p>
                    <form className={"search-form"} onSubmit={this.handleSubmit}>
                        <div className={"search-buttons"}>
                            <label>
                                <input
                                    type={"radio"}
                                    name={"searchType"}
                                    value={"books"}
                                    className={"search-radio"}
                                    id={"search-book-button"}
                                />
                                Knihu
                            </label>
                            <label>
                                <input
                                    type={"radio"}
                                    name={"searchType"}
                                    value={"users"}
                                    className={"search-radio"}
                                    id={"search-user-button"}
                                />
                                Uživatele
                            </label>
                        </div>
                        <input
                            type={"text"}
                            name={"textInput"}
                            id={"search-input"}
                        />
                        <input
                            type={"submit"}
                            value={"Vyhledat"}
                            id={"search-button"}
                        />
                    </form>
                </div>
                <fieldset>
                    <legend>Výsledky hledání</legend>
                    {searchResults}
                </fieldset>
            </main>
        )
    }
}

export default Search;