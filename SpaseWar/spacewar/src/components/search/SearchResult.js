import React from 'react'
import {BrowserRouter, Link} from "react-router-dom";

class SearchResult extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className={this.props.resultType}>
                <div className={"result-book"}>
                    <img src={this.props.imageSource} alt={""} />
                    <div className={"result-book-info"}>
                        <h1>{this.props.title}</h1>
                        <h2>{this.props.author}</h2>
                        <p>{this.props.year}</p>
                        <Link to={"/"}>Rezervovat / Vypůjčit si</Link>
                    </div>
                </div>
                <div className={"result-user-info"}>
                    <h2>{this.props.owner}</h2>
                    <Link to={"/"}>Napsat vlastníkovi</Link>
                    <Link to={"/"}>Knihovna vlastníka</Link>
                </div>
            </div>
        )
    }
}

export default SearchResult;