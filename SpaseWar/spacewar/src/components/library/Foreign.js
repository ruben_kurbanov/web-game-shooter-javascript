import React from "react";

import Book from "./utils/Book";
import BookData from "./utils/utils";

import "./../../css/library/Foreign.css"
import "../../css/library/Base.css"


export default class Foreign extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            loaded: false,
            ownerBookData: []
        }
        this.ownerName = "";
        this.ownerEmail = "";
        this.photoLink = "";
        this.chatLink = "";

        this.loadData();
    }

    componentDidMount() {
        this.setState({
            loaded: true
        })
    }

    loadData() {
        this.loadOwnerData();
        this.loadOwnerBookData();
    }

    loadOwnerData() {
        // TODO: load from db
        this.ownerName = "Jméno uživatele";
        this.ownerEmail = "E-mail uživatele";
        this.photoLink = "";
        this.chatLink = "";
    }

    loadOwnerBookData() {
        // TODO: load from db
        this.state.ownerBookData.push(BookData.createTestBookData("foreignBook", "1"))
        this.state.ownerBookData.push(BookData.createTestBookData("foreignBook", "2"))
        this.state.ownerBookData.push(BookData.createTestBookData("foreignBook", "3"))
        this.state.ownerBookData.push(BookData.createTestBookData("foreignBook", "4"))
        this.state.ownerBookData.push(BookData.createTestBookData("foreignBook", "5"))
        this.state.ownerBookData.push(BookData.createTestBookData("foreignBook", "6"))
        this.state.ownerBookData.push(BookData.createTestBookData("foreignBook", "7"))
        this.state.ownerBookData.push(BookData.createTestBookData("foreignBook", "8"))
        this.state.ownerBookData.push(BookData.createTestBookData("foreignBook", "9"))
        this.state.ownerBookData.push(BookData.createTestBookData("foreignBook", "10"))
        this.state.ownerBookData.push(BookData.createTestBookData("foreignBook", "11"))
        this.state.ownerBookData.push(BookData.createTestBookData("foreignBook", "12"))
    }

    render() {
        return (
            <main>
                <div className={"main-div"}>
                    <div className={"owner-div"}>
                        <div className={"hbox space-between"}>
                            <div className={"owner-image-div"}>
                                <img className={"owner-image"} src={this.photoLink} alt={""} />
                            </div>
                            <div className={"vbox space-around"}>
                                <span>{this.ownerName}</span>
                                <span>{this.ownerEmail}</span>
                            </div>
                            <div className={"owner-link-div"}>
                                <a className={"link-line"} href={this.chatLink}>Napsat uživateli</a>
                            </div>
                        </div>
                    </div>
                    <div className={"scroll-widget scroll-widget-big"}>
                        <b className={"scroll-widget-header-text"}>Knihy uživatele</b>
                        <div className={"wrap-box"}>
                            { this.state.ownerBookData.map(function (bookData) { return (
                                <div className={"book-place"}>
                                    <Book data={bookData}/>
                                </div>
                            )
                            })}
                        </div>
                    </div>
                </div>
            </main>
        )
    }
}