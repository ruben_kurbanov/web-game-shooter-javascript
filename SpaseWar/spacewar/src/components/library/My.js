import React from "react";

import Book from "./utils/Book";
import BookData from "./utils/utils";

import "../../css/library/Base.css"
import "../../css/library/My.css"


export default class My extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            loaded: false,
            addBookLink: "",
            myBooksData: [],
            borrowedBookData: [],
            reservedBookData: []
        }
        this.loadData();
    }

    componentDidMount() {
        this.setState({
            loaded: true
        })
    }

    loadData() {
        this.loadBooksData();
    }

    loadBooksData() {
        // TODO: add db connection for myBooks
        this.state.myBooksData.push(BookData.createTestBookData("myBook", "1"));
        this.state.myBooksData.push(BookData.createTestBookData("myBook", "2"));
        this.state.myBooksData.push(BookData.createTestBookData("myBook", "3"));
        this.state.myBooksData.push(BookData.createTestBookData("myBook", "4"));
        this.state.myBooksData.push(BookData.createTestBookData("myBook", "5"));
        this.state.myBooksData.push(BookData.createTestBookData("myBook", "6"));
        this.state.myBooksData.push(BookData.createTestBookData("myBook", "7"));

        // TODO: add db connection for borrowedBooks
        this.state.borrowedBookData.push(BookData.createTestBookData("borrowedBook", "1"));
        this.state.borrowedBookData.push(BookData.createTestBookData("borrowedBook", "2"));
        this.state.borrowedBookData.push(BookData.createTestBookData("borrowedBook", "3"));

        // TODO: add db connection for reservedBooks
        this.state.reservedBookData.push(BookData.createTestBookData("reservedBook", "1"));
        this.state.reservedBookData.push(BookData.createTestBookData("reservedBook", "2"));
        this.state.reservedBookData.push(BookData.createTestBookData("reservedBook", "3"));
        this.state.reservedBookData.push(BookData.createTestBookData("reservedBook", "4"));
        this.state.reservedBookData.push(BookData.createTestBookData("reservedBook", "5"));
    }

    render() {
        return (
            <main>
                <div className={"main-div"}>
                    <div className={"add-book-link-div"}>
                        <a className={"link-button"} href={this.state.addBookLink}>Přidat knihu</a>
                    </div>
                </div>
                <br/>
                <br/>
                <div className={"hbox space-around"}>
                    <div className={"scroll-widget scroll-widget-small"}>
                        <b className={"scroll-widget-header-text"}>Moje knihy</b>
                        { this.state.myBooksData.map(function (bookData) { return (<Book data={bookData}/>); }) }
                    </div>
                    <div className={"scroll-widget scroll-widget-small"}>
                        <b className={"scroll-widget-header-text"}>Vypůjčené knihy</b>
                        { this.state.borrowedBookData.map(function (bookData) { return (<Book data={bookData}/>); }) }
                    </div>
                    <div className={"scroll-widget scroll-widget-small"}>
                        <b className={"scroll-widget-header-text"}>Moje knihy</b>
                        { this.state.reservedBookData.map(function (bookData) { return (<Book data={bookData}/>); }) }
                    </div>
                </div>
            </main>
        )
    }
}