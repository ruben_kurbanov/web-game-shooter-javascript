/**
 * BookData => bookName (string)
 *          => authorName (string)
 *          => yearOfPublication (string)
 *          => bookState (string)
 *          => imageLink (string)
 *          => links => type (string) : (myBook|borrowedBook|reservedBook|foreignBook)
 *                   => url Array[(string)] : [ "link_1", "link_2", ... ]
 */
export default class BookData {

    constructor(bookName = "", authorName = "", yearOfPublication = "", bookState = "", imageLink = "", type = "", url = ["", "", ""]) {
        this.bookName = bookName;
        this.authorName = authorName;
        this.yearOfPublication = yearOfPublication;
        this.bookState = bookState;
        this.imageLink = imageLink;
        this.links = { type: type,
            url: url };
    }

    setLinks(type, url) {
        this.links.type = type;
        this.links.url = url;
    }

    static createTestBookData(type, id) {
        return new BookData(type + "_" + id, "authorName", "yearOfPublication", "bookState", "", type, ["", "", ""]);
    }
}

