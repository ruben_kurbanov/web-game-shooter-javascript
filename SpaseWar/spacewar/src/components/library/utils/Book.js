import React from "react";

import "../../../css/library/Base.css"
import "../../../css/library/utils/Book.css"

export default class Book extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            loaded: false,
            data: props.data
        }
    }

    componentDidMount() {
        this.setState({
            loaded: true
        })
    }

    addLinks() {
        switch (this.state.data.links.type) {
            case "myBook":
                return (
                    <div className={"hbox book-text-div"}>
                        <a className={"link-line "} href={this.state.data.links.url[0]}>Upravit</a>
                        <a className={"link-line"} href={this.state.data.links.url[1]}>Smazat</a>
                        <a className={"link-line"} href={this.state.data.links.url[2]}>Změnit stav</a>
                    </div>
                )
            case "borrowedBook":
                return (
                    <div className={"hbox book-text-div"}>
                        <a className={"link-line"} href={this.state.data.links.url[0]}>Vrátit</a>
                        <a className={"link-line"} href={this.state.data.links.url[1]}>Prodloužit</a>
                        <a className={"link-line"} href={this.state.data.links.url[2]}>Napsat vlastníkovi</a>
                    </div>
                )
            case "reservedBook":
                return (
                    <div className={"hbox book-text-div"}>
                        <a className={"link-line"} href={this.state.data.links.url[0]}>Zrušit rezervaci</a>
                        <a className={"link-line"} href={this.state.data.links.url[1]}>Napsat vlastníkovi</a>
                    </div>
                )
            case "foreignBook":
                return (
                    <div className={"hbox book-text-div"}>
                        <a className={"link-line"} href={this.state.data.links.url[0]}>Rezervovat</a>
                        <a className={"link-line"} href={this.state.data.links.url[1]}>Zapůjčit</a>
                    </div>
                )
            default:
                return;
        }
    }

    render() {
        return (
            <div className={"book-main-div"}>
                <div className={"hbox"}>
                    <div className={"book-image-div"}>
                        <img className={"book-image"} src={this.state.data.imageLink} alt={"Obal knihy"} />
                    </div>
                    <div className={"vbox space-around text-block"}>
                        <div className={"hbox book-text-div"}>
                            <span>{this.state.data.bookName}</span>
                            <span>{this.state.data.authorName}</span>
                        </div>
                        <div className={"hbox book-text-div"}>
                            <span>{this.state.data.bookState}</span>
                            <span>{this.state.data.yearOfPublication}</span>
                        </div>
                        {this.addLinks()}
                    </div>
                </div>
            </div>
        )
    }
}