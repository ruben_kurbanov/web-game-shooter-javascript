import React from 'react';
import { Link } from 'react-router-dom';

import App from '../App.js'
import '../cssComponent/Panel.css';

export default class Panel extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            loaded: false,

        }
        this.draw = this.draw.bind(this);
        this.play = this.play.bind(this);

    }

    r = 1;
    playerPositionY = 450;
    playerPositionX = 300;

    gamePositionLeft = 0;
    gamePositionRight = 650;
    gamePositionUp = 250;
    gamePositionDown = 450;



    play(){

        //this.draw(0, 0, 700,  500);
        var conv = document.getElementById("pan");
        var ctx = conv.getContext('2d');

        var panel = new Image();
        var player = new Image();
        panel.src = "./pictures/play.jpg";
        player.src = "./pictures/player.jpg"

        ctx.drawImage(panel, 0, 0, 700,  500)
        ctx.drawImage(player, this.playerPositionX, this.playerPositionY,  50,  50);

        this.draw(ctx);

        //this.draw(this.player, 0, this.playerPosition,  50,  50, ctx);
        requestAnimationFrame(this.play);
    }

    draw(ctx){
        ctx.beginPath();
        ctx.arc(this.playerPositionX + 25, this.playerPositionY, 5, 0, 2 * Math.PI);
        ctx.fillStyle = 'white';
        ctx.fill();
        ctx.stroke();
    }

    componentDidMount() {

        this.setState({
            loaded: true,
        })

        window.addEventListener('keydown',(e) => {this.KeyDown(e)});
        this.play();

    }

    KeyDown = (e) => {
        if (e.code == 'KeyW') {
            if(this.playerPositionY > this.gamePositionUp){
                this.playerPositionY -= 10;
            }

        }

        if (e.code == 'KeyS') {
            if(this.playerPositionY < this.gamePositionDown){
                this.playerPositionY += 10;
            }
        }

        if (e.code == 'KeyA') {
            if(this.playerPositionX > this.gamePositionLeft){
                this.playerPositionX -= 10;
            }
        }

        if (e.code == 'KeyD') {
            if(this.playerPositionX < this.gamePositionRight){
                this.playerPositionX += 10;
            }
        }
    }

    render() {
        return (
            <body>
                <Link to="/pause">
                    <div className="header-link_pause">
                        <h1>Pause</h1>
                    </div>
                </Link>
                <canvas id = "pan" ref={this.colorPickerRef} width = "700" height = "500"></canvas>

            </body>

        )

    }
}