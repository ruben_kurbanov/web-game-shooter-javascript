import React from 'react';
import { Link } from 'react-router-dom';

import App from '../App.js'

export default class Developed extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            loaded: false,
        }
    }

    componentDidMount() {
        this.setState({
            loaded: true
        })
    }

    render() {
        return (
            <main>
                <label>
                    Developed By
                </label>
            </main>
        )
    }
}