import React from 'react';
import { Link } from 'react-router-dom';

import App from '../App.js'

export default class Pause extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            loaded: false,
        }
    }

    componentDidMount() {
        this.setState({
            loaded: true
        })
    }

    render() {
        return (
            <main>

                <label>
                    Pause
                </label>

                <Link to="/panel">
                    <div className="header-link_panel">
                        <h1>Continuje</h1>
                    </div>
                </Link>

                <Link to="/">
                    <div className="header-link_home">
                        <h1>Back to main menu</h1>
                    </div>
                </Link>

            </main>
        )
    }
}