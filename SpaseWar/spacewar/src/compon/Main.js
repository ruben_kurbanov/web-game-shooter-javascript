import React from 'react'
import { Link } from 'react-router-dom';

import Instruction from "./Instruction";
import Settings from "./Settings";
import Panel from "./Panel";

import '../cssComponent/Main.css';

export default class Main extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            loaded: false,
        }
    }

    componentDidMount() {
        this.setState({
            loaded: true
        })
    }
    render() {

        return (
            <main>



                <Link to="/panel">
                    <button>
                        Play
                     </button>
                </Link>

                <Link to="/instruction">
                    <button>
                        Instruction
                    </button>
                </Link>

                <Link to="/settings">
                    <button>
                        Settings
                    </button>
                </Link>
            </main>

        )

    }
}
