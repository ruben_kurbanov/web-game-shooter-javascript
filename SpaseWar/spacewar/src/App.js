import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import Login from "./components/account/Login";
import Registration from "./components/account/Registration";

import Search from "./components/search/Search";
import LibraryMy from "./components/library/My";
import LibraryForeign from "./components/library/Foreign";
import Chat from "./components/account/Chat";
import Header from "./components/Header.js";
import Footer from "./components/Footer.js";

import Setting from "./compon/Settings";
import Main from "./compon/Main";
import Instruction from "./compon/Instruction";
import Panel from "./compon/Panel";
import Pause from "./compon/Pause";
import Developed from "./compon/Developed";


import logo from './logo.svg';
import './App.css';

class App extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      loaded: false,
      token: ""
    }
  }
//                  <Route exact path="/login" component={Login} />
//                  <Route exact path="/registration" component={Registration} />
//                  <Route exact path="/setting">
//                    <Setting />
//                  </Route>
//                  <Route exact path="/chat">
//                    <Chat />
//                  </Route>
//                  <Route exact path="/library/my">
//                    <LibraryMy />
//                  </Route>
//                  <Route exact path="/library/foreign">
//                    <LibraryForeign />
//                  </Route>
//                  <Route exact path="/">
//                    <Search name={"NSS"}/>
//                  </Route>
  render() {
      return (
          <div className={"App"}>
          <BrowserRouter>

              <Switch>

                  <Route exact path="/">
                    <Main />
                  </Route>

                  <Route exact path="/settings">
                    <Setting />
                  </Route>

                  <Route exact path="/instruction">
                    <Instruction />
                  </Route>

                  <Route exact path="/panel">
                    <Panel />
                  </Route>

                  <Route exact path="/pause">
                    <Pause />
                  </Route>

                  <Route exact path="/developed">
                    <Developed />
                  </Route>

              </Switch>

              <Footer />
            </BrowserRouter>
        </div>
    )
  }
}

export default App;
