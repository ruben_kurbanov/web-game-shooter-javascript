
//vyber html elementu
var context = document.getElementById("context");
var buttonPlay = document.getElementById("play");
var buttonPause = document.getElementById("pause");
var buttonContinues = document.getElementById("cont");
var buttonCancel = document.getElementById("cancel");
var buttonView = document.getElementById("view");
var slider = document.getElementById("slider");
var mesForUser = document.getElementById("mess");

//nastavim nektere nastaveni, aby nektere tlacurka nefungovali
buttonPause.disabled = true;
buttonContinues.disabled = true;
buttonCancel.disabled = true;

//vytvareni html elementu "video"
var video = document.createElement("video");
video.width = 400;
video.height = 300;
video.src = "video/p.mp4";
video.type = "video/mp4";

//vytvareni html elementu "canvas"
var canvas = document.createElement("canvas");
canvas.id = "panel";
canvas.width = 700;
canvas.height = 500;

//definujeme canvas pro praci s nim
var ctx = canvas.getContext('2d');

//ulozeni constantnich obrazovek
var panel = new Image();
var player = new Image();
panel.src = "pictures/play.jpg";
player.src = "pictures/player.jpg"

//ulozeni audio
var shot = new Audio()
shot.src = "audio/shot.mp3";

var hit = new Audio()
hit.src = "audio/hit.mp3";

var died = new Audio()
died.src = "audio/died.mp3";

var game = new Audio()
game.src = "audio/game.mp3";

//promene pro pouzivani ve hre
var timer;
var player;
var bulletsPlayer;
var livesPlayer;
var enemys;
var wawe;
var isPushStop;
var b;
var timerId;

//pridava se listenery
document.addEventListener('keydown',(e) => {this.KeyDown(e)});// pri stisknuti klavesnici

document.addEventListener("resize", Resize);//pri obnoveni okna

slider.addEventListener("change", function() { // pri zmene zvuka sliderem

	setVolume(this.value/100);

});

video.addEventListener('ended', function(){ // pri ukonceni videa, html element video se odstrani y contextu
	context.removeChild(video);
	buttonPlay.disabled = false;
});

//spracovava pri obnoveni stranky
if (window.performance) {

	//jestli uzivatel zatim nezadal svoje jmeno, pri obnoveni web stranky, web pozada o vplneni formulare
	if (window.localStorage.getItem("nameUser") ==  null) {

			//pozada o vyplneni formulare
			let nameUser = prompt('What is your name?', '');
			//ulozi jmeno uzivatele na localStarage
			window.localStorage.setItem("nameUser", nameUser);

	}

	// vypise uvitaci zpravu
	mesForUser.innerText = "Hi, " + window.localStorage.getItem("nameUser")+ "! Are you ready?)";

}

//jestli uzivatel stiskne tlacitko mute, vypne se zvuk
function mute(){
	slider.value = 0;
	setVolume(0);
}

//jestli uzivatel stiskne tlacitko unmute, obnovi se zvuk
function unmute(){

	let val = slider.value;
	if(val == 0){
		slider.value = 50;
		setVolume(0.5);
	}
	
}

//jestli uzivatel stiskne tlacitko view video, zobrazi se video
function viewVideo(){
	context.appendChild(video); // do contextu se prida html prvek video
	video.play(); //zacne prehravani
	video.volume = slider.value/100; //nasaveni zvuka
	buttonPlay.disabled = true;
}

//stisknuti tlacitka play
function play(){
	context.appendChild(canvas); // do contextu se prida html element canvas

	//inicializace prvku hry
	player = new Boads("pictures/player.jpg", canvas.width/2, canvas.height, 60, 10);
	bulletsPlayer = [];
	livesPlayer = [];
	enemys =[];
	timer = null;
	wawe = new Wawe();
	isPushStop = false;
	
	b = false;
	timerId = null;

	// nastavi se zvuk hry
	let vol = slider.value/100; 
	shot.volume = vol;
	hit.volume = vol;
	died.volume = vol;
	game.volume = vol;

	buttonPlay.disabled = true;
	buttonView.disabled = true;

	Start(); // zacne bezet hra
}

//do canvasu se vypise zprava o stavu hry
function drawWawe(str){

	ctx.drawImage(panel, 0, 0, canvas.width,  canvas.height);
	ctx.font = '100px Viranda';
	ctx.strokeStyle = "blue";
	ctx.lineWidth = 5;
	ctx.strokeText(str, 280, 250);

}

//stisk tlacitka Pause
function pause(){

	buttonPause.disabled = true;
	buttonContinues.disabled = false;

	Stop(); // zastavi se hra
	drawWawe("Pause...");
	game.pause(); // zastavi se audio
}

//stisk tlacitka Continue
function continueGame(){

	buttonPause.disabled = false;
	buttonContinues.disabled = true;

	Start();
}

//stick tlacitka Cancel. Hra je ukoncena
function cancel(){

	buttonCancel.disabled = true;
	buttonContinues.disabled = true;
	buttonPause.disabled = true;

	Stop(); // zastaveni hry

	//vynulovani prvku hry
	bulletsPlayer = [];
	livesPlayer = [];
	enemys =[];
	timer = null;
	wawe = new Wawe();
	isPushStop = false;
	b = false;
	timerId = null;

	drawWawe("Push Play!");//do canvasu se vypise zprava o stavu hry

	buttonPlay.disabled = false;
	buttonView.disabled = false;

	game.pause(); // zastavi se audio
	context.removeChild(canvas); // odstraneni html prvku canvas
}

//nastavi zvuk vsech prvku hrz
function setVolume(vol){
	shot.volume = vol;
	hit.volume = vol;
	died.volume = vol;
	game.volume = vol;
	video.volume = vol;
}

//generuje nahodne cislo
function RandomInteger(min, max) {
	let rand = min - 0.5 + Math.random() * (max - min + 1);
	return Math.round(rand);
}

//obnovi se zivot player
function updateLives(){

	livesPlayer = [];

	for (var i = 0; i < 10; i++) {
		livesPlayer.push(new Lives("pictures/heart.jpg", i));
		livesPlayer[i].update();
	}

}

// zacina proscess hry
function Start(){
  	timer = setInterval(Update, 1000 / 60); //Stav hry se obnovi 60 krat v sekundu 
}

//zastavi obnoveni hry
function Stop(){
	
    clearInterval(timer); //zastavi interval obnoveni hry
    clearInterval(timerId); //zastavi interval vytvareni bullets pro enemys
  
    timerId = null;
    timer = null;

    if(enemys.length != 0)  isPushStop = true; // potrebuji pro zjisteni, ze hra byla zastavena v hvili hry, pak obnovim interval pro vytvareni bullets pro enemys
}

function pauseWawe(){
    clearInterval(timer); //Остановка обновления
    clearInterval(timerId);
    
    timerId = null;
    timer = null;
}

//generace bullets pro enemys
function rr(){
	for (var i = 0; i < enemys.length; i++) {
		enemys[i].bullet.push(new Bullet("pictures/bullet.png", enemys[i].x + (enemys[i].dim/2) - 10, enemys[i].y, 5, "enemy"));
	}
	
}

//nastaveni intervalu pro bullets pro enemys
function d(){
	timerId = setInterval(rr, 2000);
}

function Update(){	

	game.play(); // aktualizace zvuku hry

	//udate new wave
	if (enemys.length == 0) {

		buttonPause.disabled = true;
		buttonCancel.disabled = true;

		//uzivatel vyhral
		if (wawe.countOfWave == 10) {
			pauseWawe()
			drawWawe("You win!");
			setTimeout(cancel,3000);
			return;
		}

		//zacatek hry
		if(wawe.countOfWave == 0){
			pauseWawe()
			drawWawe("Start");

		}else{ // jenom novy wave
			pauseWawe()
			drawWawe("Wawe " + (wawe.countOfWave + 1));
		}

		//nova uroven hry zacne pres 2 sekundy
		setTimeout(() => {

			addEnemys(wawe.countOfWave); // kolik enemys se vytvori v urcitou urovni
			wawe.update();
			updateLives(); // obnovi se zivot playeru
			b = true; // zacala se nova uroven a obnovim interval pro bullets pro enemys
			Start(); // zacina hra
			buttonPause.disabled = false;
			buttonCancel.disabled = false;
		
		}, 2000);

		return;
	}

	//player prohral
	if(livesPlayer == 0){
		died.play(); // zvuk explodujici lod
		Stop(); // zastavi se hra
		drawWawe("You lose!");
		setTimeout(cancel ,3000); // odstrani se game panel pres 3 sekundy
		return;
	}

	//obnoveni bullets pro player
	for (var i = 0; i < bulletsPlayer.length; i++){

    	bulletsPlayer[i].update();

    	//jestli bullet zasel za hranici panelu - > odstrani se
    	if (bulletsPlayer[i].outFields()) {
    		bulletsPlayer.splice(i, 1);
    	}
	}


	for (var i = 0; i < enemys.length; i++) {

		enemys[i].update(); //obnovi polohu enemys

		//jestli byla nova uroven, obnovi se interval pro bullets pro enemys
		if (b) {
			d();
			b = false;
		}

		//jestli byl stisknut tlacitko pause, obnovi se interval pro bullets pro enemys
		if (isPushStop) {
			d();
			isPushStop = false;
		}
		
		//kontrola porazeni enemy bullets player
		for (var j = 0; j < bulletsPlayer.length; j++) {

			var	 dx = enemys[i].x + 30 - bulletsPlayer[j].x;
    		var dy = enemys[i].y - bulletsPlayer[j].y - 20;
    		var dist = Math.sqrt(dx*dx + dy*dy);

    		if(dist < enemys[i].dim/2) {
    			bulletsPlayer.splice(j, 1);
    			hit.play();
				enemys[i].hit();
				
    		}

		}

		//kontrola jestli bullet enemy porazi player
		for (var k = 0; k < enemys[i].bullet.length; k++) {
			enemys[i].bullet[k].update();

			var bx = enemys[i].bullet[k].x;
 			var by = enemys[i].bullet[k].y;
 
 			var dx = player.x - bx + 30;
 			var dy = player.y - by;
 			var dist = Math.sqrt(dx*dx + dy*dy);

 			if(dist < player.dim/2) {
 				
 				enemys[i].bullet.splice(k, 1);
 				hit.play();
 				livesPlayer.pop();
 				
 			}

 			//jestli bullet zasel za hranici panelu - > odstrani se
 			if (enemys[i].bullet[k] != null && enemys[i].bullet[k].outFields()) {
				enemys[i].bullet.splice(k, 1);
			}

		}

		//kontrola, jestli enemy ma lives, jinak se odstrani ze hry
		if(enemys[i].lives < 1) {
			died.play();
			enemys.splice(i, 1);
		}
	}

	//kresleni hry
	Draw();
}   

//vytvari nove enemys
function createEnemy(image, dim, lives, speed){

	return new Enemy(image,  RandomInteger(dim, canvas.width - dim), dim, dim, lives, speed);
}


//podle urovne kontroluje kolik enemys a jakeho typu pro uroven vytvorit
function addEnemys(countOfWave){
	switch(countOfWave){
		case 0:
			enemys.push(createEnemy("pictures/enemy.jpg", 60, 3, 1));
			break;

		case 1:
			for (var i = 0; i < 3; i++) {
				enemys.push(createEnemy("pictures/enemy.jpg", 60, 3, 1));
			}
			break;

		case 2:
			for (var i = 0; i < 6; i++) {
				enemys.push(createEnemy("pictures/enemy.jpg", 60, 3, 1));
			}
			break;

		case 3:
			for (var i = 0; i < 10; i++) {
				enemys.push(createEnemy("pictures/enemy.jpg", 60, 3, 1));
			}
			break;

		case 4:
			enemys.push(createEnemy("pictures/enemy2.jpg", 80, 10, 2));
			break;
		case 5:
			for (var i = 0; i < 3; i++) {
				enemys.push(createEnemy("pictures/enemy.jpg", 60, 3, 1));
			}
			enemys.push(createEnemy("pictures/enemy2.jpg", 80, 10, 2));
			break;

		case 6:
			for (var i = 0; i < 3; i++) {
				enemys.push(createEnemy("pictures/enemy2.jpg", 80, 10, 2));
			}
			break;
		case 7:
			for (var i = 0; i < 3; i++) {
				enemys.push(createEnemy("pictures/enemy.jpg", 60, 3, 1));
			}
			for (var i = 0; i < 3; i++) {
				enemys.push(createEnemy("pictures/enemy2.jpg", 80, 10, 2));
			}
			break;

		case 8:
			for (var i = 0; i < 6; i++) {
				enemys.push(createEnemy("pictures/enemy2.jpg", 80, 10, 2));
			}
			for (var i = 0; i < 3; i++) {
				enemys.push(createEnemy("pictures/enemy.jpg", 60, 3, 1));
			}
			break;

		case 9:
			for (var i = 0; i < 7; i++) {
				enemys.push(createEnemy("pictures/enemy2.jpg", 80, 10, 2));
			}
			for (var i = 0; i < 8; i++) {
				enemys.push(createEnemy("pictures/enemy.jpg", 60, 3, 1));
			}
			break;

	}
}

//kresleni hry
function Draw(){
    ctx.clearRect(0, 0, canvas.width, canvas.height); //cisteni hraci panel

    ctx.drawImage(panel, 0, 0, canvas.width,  canvas.height);// kresleni backspase pro hro

	//kresleni hrace
	ctx.drawImage
	    (
	        player.image, 
	        player.x, 
	        player.y,
	        player.dim,
	        player.dim, 
	    );

	//kresleni bullets pro player
	for (var i = 0; i < bulletsPlayer.length; i++) {
		ctx.drawImage
	    (
	        bulletsPlayer[i].image, 
	        bulletsPlayer[i].x, 
	        bulletsPlayer[i].y,
	        bulletsPlayer[i].width ,
	        bulletsPlayer[i].height ,
	    );
	}

	//kresleni zivota pro player
	for (var i = 0; i < livesPlayer.length; i++) {

		ctx.drawImage
	    (
	        livesPlayer[i].image, 
	        livesPlayer[i].x, 
	        livesPlayer[i].y,
	        livesPlayer[i].width ,
	        livesPlayer[i].height ,
	    );
	}

	//kresleni enemys
	for (var i = 0; i < enemys.length; i++) {

		ctx.drawImage
	    (
	        enemys[i].image, 
	        enemys[i].x, 
	        enemys[i].y,
	        enemys[i].dim ,
	        enemys[i].dim ,
	    );

	    //kresleni bullets pro enemys
		for (var j = 0; j < enemys[i].bullet.length; j++) {

			ctx.drawImage
	    	(
				enemys[i].bullet[j].image, 
		        enemys[i].bullet[j].x, 
		        enemys[i].bullet[j].y,
		        enemys[i].bullet[j].width ,
		        enemys[i].bullet[j].height ,
	        );
		}

	}

}

//listener pro pohyb a akt player
KeyDown = (e) => {

	//nahoru
    if (e.code == 'KeyW') {
       player.Move("y", -5);
    }

    //dolu
    if (e.code == 'KeyS') {
        player.Move("y", 5);
    }

    //vlevo
    if (e.code == 'KeyA') {
        player.Move("x", -5);
    }

    //vpravo
    if (e.code == 'KeyD') {
        player.Move("x", 5);
    }

    //vystrelit
    if (e.code == 'Space') {

    	if(bulletsPlayer.length < 10){
    		shot.play();
    		bulletsPlayer.push(new Bullet("pictures/bullet.png", player.x + (player.dim/2) - 10, player.y, 10, "player"));
    	}
    }
}

function Resize(){
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;
}
