class Lives{

	constructor(image, i){

		this.image = new Image();
		this.image.src = image;

		this.width = 20;
		this.height = 20;

		this.dist = 5;
		this.i = i;

		this.x = 450;
		this.y = 10;
	}

	update(){
		this.x += (this.width + this.dist)*this.i;
	}
}