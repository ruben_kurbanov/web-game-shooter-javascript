class Enemy{

	constructor(image, x, y, dim, lives, speed)
    {
        this.x = x - dim;
        this.y = y - dim;

        this.dim = dim;

        this.lives = lives;
 
        this.image = new Image();
 
        this.image.src = image;

        this.angle = Math.tan(RandomInteger(0, 360));
        this.dx = Math.sin(this.angle) * speed;
        this.dy = Math.cos(this.angle) * speed;

        this.bullet = [];
    }


    hit(){this.lives--;}

    update(){

		this.x += this.dx;
	    this.y += this.dy;
	      
	    if(this.x < 0 && this.dx < 0) this.dx = -this.dx;
	    if(this.x > canvas.width - this.dim && this.dx > 0) this.dx = -this.dx;
	    if(this.y < this.dim/2 && this.dy < 0) this.dy = -this.dy;
	    if(this.y > canvas.height/2 - this.dim && this.dy > 0) this.dy = -this.dy;
    }

}