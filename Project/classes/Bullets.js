class Bullet{

	constructor(image, x, y, speed, who){

		this.x = x;
		this.y = y;
		this.speed = speed;
		this.who = who;

		this.image = new Image();
 
        this.image.src = image;

        this.width = 20;
        this.height = 40;

	}

	outFields(){
		if (this.y < 0 || this.y > canvas.height) { return true;}
	}

	update(){

		if (this.who == "player") {this.y -= this.speed;}
		else{this.y += this.speed;}
	}


}